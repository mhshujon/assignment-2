<?php
include_once '../../src/Methods.php';
//var_dump($_POST);
$_POST['id'] = $_GET['id'];

$object = new Methods();

if (isset($_POST['remove']) && $_POST['remove'] == 'remove'){
    $object->delete($_GET['id']);
    $_GET['nav'] = 'show-all';
    header('location: ../user/show-all.php');
}
elseif (isset($_POST['update']) && $_POST['update'] == 'update'){
    $object->set($_POST);
    $object->update($_GET['id']);
    $_GET['nav'] = 'show-all';
    header('location: ../user/show-all.php');
}

