<?php
$_GET['nav'] = 'show-all';
include_once '../include/header.php';
include_once '../../src/Methods.php';

$object = new Methods();

$data = $object->index();

//var_dump($data)
?>

    <div class="container" style="margin-top: 50px">
        <div class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered table-striped mb-0" style="text-align: center">
                <thead>
                <tr>
                    <th scope="col" style="width: 5px">#Serial</th>
                    <th scope="col">Data Type</th>
                    <th scope="col">Variable Name</th>
                    <th scope="col">Value</th>
                    <th scope="col">Customize</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($row=0; $row < count($data); $row++):?>
                    <form action="view/admin/customize.php?id=<?php echo $data[$row]['id']?>" method="POST">
                        <tr>
                            <th scope="row"><?php echo $row+1?></th>
                            <td contenteditable="true">
                                <input type="text" value="<?php echo $data[$row]['type']?>" name="type">
                            </td>
                            <td contenteditable="true">
                                <input type="text" value="<?php echo $data[$row]['name']?>" name="name">
                            </td>
                            <td contenteditable="true">
                                <input type="text" value="<?php
                                if ($data[$row]['value'] == NULL)
                                    echo 'NULL';
                                else
                                    echo $data[$row]['value']
                                ?>" name="value">
                            </td>
                            <td>
                                <span>
                                    <button type="submit" class="btn btn-danger btn-rounded btn-sm my-0" name="remove" value="remove">Remove</button>
                                </span>
                                <span>
                                    <button type="submit" class="btn btn-success btn-rounded btn-sm my-0" name="update" value="update">Update</button>
                                </span>
                            </td>
                        </tr>
                    </form>
                <?php endfor;?>
                </tbody>
            </table>
        </div>
    </div>

<?php include_once '../include/footer.php' ?>