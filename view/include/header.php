<?php
session_start();
if (isset($_SESSION['validity'])){
    if ($_SESSION['validity'] == 'yes'){
        echo "<script>window.alert('Valid Input')</script>";
        session_destroy();
    }
    elseif ($_SESSION['validity'] == 'no'){
        echo "<script>window.alert('Invalid Input')</script>";
        session_destroy();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Compiler Assignments</title>
    <base href="http://localhost/assignment-2/">
    <!-- Font Awesome -->
    <!--    <link rel="stylesheet" href="assets/font/all.css">-->
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark primary-color">

    <div class="container" style="margin-left: 700px; margin-top: 25px; margin-bottom: 25px">
        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

            <!-- Links -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php if ($_GET['nav'] == 'insert'){echo 'active';}?>">
                    <a class="nav-link" href="index.php">Insert
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($_GET['nav'] == 'show-all'){echo 'active';}?>">
                    <a class="nav-link" href="view/user/show-all.php">Show All</a>
                </li>

                <form class="form-inline">
                    <div class="md-form my-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Find" aria-label="Find">
                    </div>
                </form>
        </div>
        <!-- Collapsible content -->
    </div>

</nav>
<!--/.Navbar-->