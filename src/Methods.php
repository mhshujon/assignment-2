<?php
include_once 'Connection.php';

class Methods extends Connection
{
    private $id = NULL;
    private $type;
    private $typeName;
    private $value;

    public function dataSeparation($data){
        $data = $data;
        $flagComma = 0;
        $flagEqual = 0;

        $typeSeparate = explode(" ", $data);

        $this->type = $typeSeparate[0];

        $splitArray = str_split($typeSeparate[1]);
        array_pop($splitArray);

        for ($i=0; $i < count($splitArray); $i++){
            if ($splitArray[$i] == ','){
                $commaExist = implode('', $splitArray);
                $commaSeparate = explode(',', $commaExist);
                $flagComma = 1;
                break;
            }
        }

        if ($flagComma == 1){
            for ($i=0; $i < count($splitArray); $i++){
                if ($splitArray[$i] == '='){
                    $flagEqual = 1;
                    break;
                }
            }
            if ($flagEqual == 1){
                for ($i=0; $i < count($commaSeparate); $i++){
                    $equalSeparate[$i] = explode('=', $commaSeparate[$i]);
                }
                for ($i=0; $i < count($equalSeparate); $i++){
                    $this->typeName[$i] = $equalSeparate[$i][0];
                    $this->value[$i] = $equalSeparate[$i][1];
                }
            }
            else{
                for ($i=0; $i < count($commaSeparate); $i++){
                    $this->typeName[$i] = $commaSeparate[$i];
                    $this->value[$i] = NULL;
                }
            }
        }
        else{
            for ($i=0; $i < count($splitArray); $i++){
                if ($splitArray[$i] == '='){
                    $flagEqual = 1;
                    break;
                }
            }

            if ($flagEqual == 1){
                $equalSeparate = explode('=', implode('', $splitArray));
                $this->typeName = $equalSeparate[0];
                $this->value = $equalSeparate[1];
            }
            else{
                $this->typeName = $splitArray;
                $this->value = NULL;
            }
        }
//        echo $this->type;echo '<br>';
//        var_dump($this->typeName);echo '<br>';
//        var_dump($this->value);echo '<br>';
//        var_dump($equalSeparate);echo '<br>';
//        var_dump($splitArray);echo '<br>';
//        echo count($equalSeparate);

    }

    public function set($data = array()){
        if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
        if (array_key_exists('name', $data)) {
            $this->typeName = $data['name'];
        }
        if (array_key_exists('value', $data)) {
            $this->value = $data['value'];
        }
//        echo $this->type; echo '<br>';
//        echo $this->typeName; echo '<br>';
//        echo $this->value; echo '<br>';
    }

    public function store(){
        try{
            for ($i=0; $i < count($this->typeName); $i++){
                $stmt = $this->con->prepare("INSERT INTO `data` (`id`, `name`, `type`, `value`) VALUES (:id, :name, :type, :value)");
                $stmt->execute(array(
                    ':id' => $this->id,
                    ':name' => $this->typeName[$i],
                    ':type' => $this->type,
                    ':value' => $this->value[$i]
                ));
            }

//            var_dump('this->postID');
//           echo "\nPDOStatement::errorInfo():\n";
//           $arr = $stmt->errorInfo();
//           print_r($arr);
//            if($result){
//                header('postDetails:index.php');
//                echo $this->postID;

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `data`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $stmt1 = $this->con->prepare("DELETE FROM `data` WHERE id=:id");
            $stmt1->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt1->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr1 = $stmt1->errorInfo();
//            $arr2 = $stmt2->errorInfo();
//            print_r($arr1);
//            print_r($arr2);
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('postDetails:index.php');
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function update($id){
        try{
            $stmt = $this->con->prepare("UPDATE `db_comp_assignment_2`.`data` SET `name` = :name, `type` = :type, `value` = :value WHERE `id` = $id;");

            $stmt->execute(array(
                ':name' => $this->typeName,
                ':type' => $this->type,
                ':value' => $this->value
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
////                echo 'Yes'.$this->email;
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}