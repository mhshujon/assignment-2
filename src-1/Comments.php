<?php
include ('Connection.php');

class Comments extends Connection
{
    private $commentID;
    private $postID;
    private $usersEmail;
    private $comments;

    public function set($data = array()){
        if (array_key_exists('commentID', $data)) {
            $this->commentID = $data['commentID'];
        }
        if (array_key_exists('postID', $data)) {
            $this->postID = $data['postID'];
        }
        if (array_key_exists('usersEmail', $data)) {
            $this->usersEmail = $data['usersEmail'];
        }
        if (array_key_exists('comments', $data)) {
            $this->comments = $data['comments'];
        }
////        var_dump($this);
//        return $this->postID;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `comments` (`commentID`, `postID`, `usersEmail`, `comments`) VALUES (:commentID, :postID, :usersEmail, :comments)");
            $result =  $stmt->execute(array(
                ':commentID' => $this->commentID,
                ':postID' => $this->postID,
                ':usersEmail' => $this->usersEmail,
                ':comments' => $this->comments
            ));

//            var_dump('this->postID');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('postDetails:index.php');
//                echo $this->postID;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `comments`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function updatePost(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`comments` SET `commentID` = :commentID, `postID` = :postID, `usersEmail` = :usersEmail, `comments` = :comments, `hospital` = :hospital, `contactPerson` = :contactPerson, `contactNumber` = :contactNumber, `postDetails` = :postDetails WHERE `commentID` = :commentID AND `postID` = :postID;");

            $result =  $stmt->execute(array(
                ':commentID' => $this->commentID,
                ':postID' => $this->postID,
                ':usersEmail' => $this->usersEmail,
                ':comments' => $this->comments,
                ':hospital' => $this->hospital,
                ':contactPerson' => $this->contactPerson,
                ':contactNumber' => $this->contactNumber,
                ':postDetails' => $this->postDetails
            ));
            echo "\nPDOStatement::errorInfo():\n";
            $arr = $stmt->errorInfo();
            print_r($arr);
//            if($result){
////                echo 'Yes'.$this->email;
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function login($commentID, $usersEmail){
        try{
            $stmt = $this->con->prepare("SELECT commentID, usersEmail FROM `comments` WHERE `commentID`='$commentID' AND `usersEmail`='$usersEmail'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `comments`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function commentID_validation($commentID){
        try{
            $stmt = $this->con->prepare("SELECT commentID FROM `comments` WHERE `commentID`='$commentID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `comments` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `comments` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('postDetails:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`comments` SET `firstpostID` = :firstpostID, `lastpostID` = :lastpostID, `commentID` = :commentID, `postID` = :postID, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `commentID` = :commentID;");

            $result =  $stmt->execute(array(
                ':firstpostID' => $this->firstpostID,
                ':lastpostID' => $this->lastpostID,
                ':commentID' => $this->commentID,
                ':postID' => $this->postID,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->commentID;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}