<?php
include ('Connection.php');

class Posts extends Connection
{
    private $postID;
    private $usersEmail;
    private $bloodGroupNeeded;
    private $donorNeeded;
    private $hospital;
    private $contactPerson;
    private $contactNumber;
    private $postDetails = NULL;

    public function set($data = array()){
        if (array_key_exists('postID', $data)) {
            $this->postID = $data['postID'];
        }
        if (array_key_exists('usersEmail', $data)) {
            $this->usersEmail = $data['usersEmail'];
        }
        if (array_key_exists('bloodGroupNeeded', $data)) {
            $this->bloodGroupNeeded = $data['bloodGroupNeeded'];
        }
        if (array_key_exists('donorNeeded', $data)) {
            $this->donorNeeded = $data['donorNeeded'];
        }
        if (array_key_exists('hospital', $data)) {
            $this->hospital = $data['hospital'];
        }
        if (array_key_exists('contactPerson', $data)) {
            $this->contactPerson = $data['contactPerson'];
        }
        if (array_key_exists('contactNumber', $data)) {
            $this->contactNumber = $data['contactNumber'];
        }
        if (array_key_exists('postDetails', $data)) {
            $this->postDetails = $data['postDetails'];
        }
////        var_dump($this);
//        return $this->usersEmail;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `posts` (`postID`, `usersEmail`, `bloodGroupNeeded`, `donorNeeded`, `hospital`, `contactPerson`, `contactNumber`, `postDetails`) VALUES (:postID, :usersEmail, :bloodGroupNeeded, :donorNeeded, :hospital, :contactPerson, :contactNumber, :postDetails)");
            $result =  $stmt->execute(array(
                ':postID' => $this->postID,
                ':usersEmail' => $this->usersEmail,
                ':bloodGroupNeeded' => $this->bloodGroupNeeded,
                ':donorNeeded' => $this->donorNeeded,
                ':hospital' => $this->hospital,
                ':contactPerson' => $this->contactPerson,
                ':contactNumber' => $this->contactNumber,
                ':postDetails' => $this->postDetails
            ));

//            var_dump('this->usersEmail');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('postDetails:index.php');
//                echo $this->usersEmail;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `posts`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function updatePost(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`posts` SET `postID` = :postID, `usersEmail` = :usersEmail, `bloodGroupNeeded` = :bloodGroupNeeded, `donorNeeded` = :donorNeeded, `hospital` = :hospital, `contactPerson` = :contactPerson, `contactNumber` = :contactNumber, `postDetails` = :postDetails WHERE `postID` = :postID AND `usersEmail` = :usersEmail;");

            $result =  $stmt->execute(array(
                ':postID' => $this->postID,
                ':usersEmail' => $this->usersEmail,
                ':bloodGroupNeeded' => $this->bloodGroupNeeded,
                ':donorNeeded' => $this->donorNeeded,
                ':hospital' => $this->hospital,
                ':contactPerson' => $this->contactPerson,
                ':contactNumber' => $this->contactNumber,
                ':postDetails' => $this->postDetails
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
////                echo 'Yes'.$this->email;
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function login($postID, $bloodGroupNeeded){
        try{
            $stmt = $this->con->prepare("SELECT postID, bloodGroupNeeded FROM `posts` WHERE `postID`='$postID' AND `bloodGroupNeeded`='$bloodGroupNeeded'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `posts`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function postID_validation($postID){
        try{
            $stmt = $this->con->prepare("SELECT postID FROM `posts` WHERE `postID`='$postID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `posts` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
    public function delete($postID){
        try{
            $stmt1 = $this->con->prepare("DELETE FROM `posts` WHERE postID=:postID");
            $stmt1->bindValue(':postID', $postID, PDO::PARAM_INT);
            $stmt1->execute();
            $stmt2 = $this->con->prepare("DELETE FROM `comments` WHERE postID=:postID");
            $stmt2->bindValue(':postID', $postID, PDO::PARAM_INT);
            $stmt2->execute();
            echo "\nPDOStatement::errorInfo():\n";
            $arr1 = $stmt1->errorInfo();
            $arr2 = $stmt2->errorInfo();
            print_r($arr1);
            print_r($arr2);
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('postDetails:index.php');
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}